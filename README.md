# Diachronic analogies

This repository contains code for the methods discussed in Orlikowski M, Hartung M, Cimiano P. Learning Diachronic Analogies to Analyze Concept Change. In: Proceedings of the 2nd Joint SIGHUM Workshop on Computational Linguistics for Cultural Heritage, Social Sciences, Humanities and Literature (LaTeCH-CLfL 2018). Association for Computational Linguistics. URL: http://aclweb.org/anthology/W18-4500

## Data

### Evaluation data

The generated analogies as used in the reported experiments are part of the repository
(`data/analogies/`). The original data from Kenter et al. (2015) can be found
here: http://ilps.science.uva.nl/resources/shifts/

### Embeddings

https://drive.google.com/drive/folders/1DKErVk_-kfLyaPjMTe2AuCDG4ZgFH_Wu?usp=sharing

## Code overview

`notebooks/` contains all Jupyter notebooks used for tentative API
requests, embedding training and alignment, generating diachronic
analogies from the Kenter et al. data, pre-filtering the
analogies based on the available vocabulary, generating `.ini`config
files for experiments and calculating as well as visualizing the results.

`scraper/` contains a
Scrapy-based scraper for the Koninklijke Bibliotheek Newspapers
Archive.

`scripts/` contains the code for (textual) preprocessing, the models,
training and evaluation. The entry point is `experiment.py`, which can
be called with two different arguments either 1) a path to an `.ini`
file (see `example.ini`) or 2) as `python experiment.py all`. In the
second case, all config files in `scripts/configs/` are run
consecutively.
The directory also contains two `.txt` files with exemplary errors of
the transformation model and the weighted linear combination.

## Exemplary setup
* Clone the repository
* Create a Python 3 virtualenv in the project directory
* `source bin/activate`
* Adapt the PyTorch `whl`URL in `requirements.txt` to your platform
  (https://pytorch.org/previous-versions/). The code was written using
  version `0.2.0`.
* `pip install -r requirements.txt`
* To install a kernel that makes the virtualenv available in the  Jupyter notebooks do `python -m ipykernel install --user --name diachronic-analogies --display-name "Python (diachronic-analogies)"`
