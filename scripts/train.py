# -*- coding: utf-8 -*-
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable

from models import IndividuallyWeightedOffset, TargetWord, Transformation

def save_model(model, model_path):
    torch.save(model.state_dict(), model_path)

def train(train_set, model_path, epochs=1, model_name=None):
    if model_name == 'nn_baseline':
        return TargetWord()
    elif model_name == 'transformation':
        model = Transformation(300)
    else:
        model = IndividuallyWeightedOffset(300, random=False)

    criterion = nn.CosineEmbeddingLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    loss_sum = 0.0
    for epoch in range(epochs):
        for i, data in enumerate(train_set, 0):
             optimizer.zero_grad()
             if model_name == 'transformation':
                 label = data[1]
                 y = model(Variable(data[0]))
             else:
                 label = data[3]
                 y = model(Variable(data[0]), Variable(data[1]), Variable(data[2]))
             loss = criterion(y, Variable(label), Variable(torch.ones(data[0].size()[0])))
             loss.backward()
             optimizer.step()
             loss_sum += loss.data[0]

        # print statistics
        print('[{0}] loss_sum: {1:.5f}'.format(epoch + 1, loss_sum))
        loss_sum = 0.0

    return model
