import torch
from torch.nn import MSELoss, CosineEmbeddingLoss
from torch.autograd import Variable
from models import IndividuallyWeightedOffset, Transformation

def load_model(model_path, name=None):
    if name == 'transformation':
        model = Transformation(300)
    else:
        model = IndividuallyWeightedOffset(300)
    model.load_state_dict(torch.load(model_path))
    return model

def calc_reciprocal_rank(ranked_predictions, label):
    try:
        rank = ranked_predictions.index(label) + 1
    except ValueError:
        return 0
    return 1 / rank

def evaluate(test_set, embeddings, model):
    correct = 0
    reciprocal_ranks = []
    sum_squared_error = 0
    sum_cosine_distance = 0

    MSE = MSELoss(size_average = False)
    COS = CosineEmbeddingLoss(size_average = False)

    is_transform = isinstance(model, Transformation)

    for i, data in enumerate(test_set, 0):
        if is_transform:
            y = model(Variable(data[2]))
        else:
            y = model(Variable(data[0]), Variable(data[1]), Variable(data[2]))

        predicted_indecies = [[embeddings.vocab[word].index
                               for word, _ in embeddings.similar_by_vector(vector.data.numpy(), topn=10)]
                              for vector in y]
        correct_predictions = [predicteds[0] for predicteds, label in zip(predicted_indecies, data[4]) if predicteds[0] == label]
        errors = [(predicteds, label) for predicteds, label in zip(predicted_indecies, data[4]) if predicteds[0] != label]
        if len(errors) > 0:
            for predicteds, label in errors:
                predicted_term = embeddings.index2word[predicteds[0]]
                label_term = embeddings.index2word[label]
                terms = [embeddings.index2word[predicted] for predicted in predicteds[1:]]
                similarity = embeddings.similarity(predicted_term, label_term)
                with open('errors.txt', 'a+') as f:
                    print(predicted_term, "(Label: {0})".format(label_term), similarity, terms, file=f)
        correct += len(correct_predictions)
        label_vector = data[3]
        sum_squared_error += MSE(y, Variable(label_vector)).data[0] # sum all squared errors, later average over all observations
        sum_cosine_distance += COS(y, Variable(label_vector), Variable(torch.ones(data[0].size()[0]))).data[0]
        reciprocal_ranks += [calc_reciprocal_rank(predicted_indecies, label) for predicted_indecies, label in zip(predicted_indecies, data[4])]
    N = len(reciprocal_ranks)
    mrr = sum(reciprocal_ranks) / N
    mse = sum_squared_error / N
    cos = 1 - (sum_cosine_distance / N) # 1 - mean_cosine_distance = mean_cosine_similarity
    return {'correct': correct,
            'mean_reciprocal_rank': mrr,
            'mean_squared_error': mse,
            'mean_cosine_similarity': cos}
