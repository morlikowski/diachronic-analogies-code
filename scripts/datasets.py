from torch.utils.data import Dataset
import os

class FieldConstants():
    def __init__(self):
        self.A = 0
        self.B = 1
        self.C = 2
        self.D = 3

FIELDS = FieldConstants()


def generate_ranked_labels(token, embeddings, top_n=9):
    ranked_labels = [embeddings.vocab[token].index]
    ranked_labels += [embeddings.vocab[token].index for token, score
                                        in embeddings.most_similar(token, topn=top_n)]
    return ranked_labels

"""
Loads an analogy data set. Expects a single file with four analogy terms per line, each
delimited by a delimiter symbol. The analogy is expected to be in
"A is to B as C is to D" order. Lines starting with a colon are filtered out.
"""
class Analogies(Dataset):
    def __init__(self,
                 path,
                 word_vectors,
                 delimiter=' ',
                 preprocess=lambda token: token):
        """
        Args:
        path (string): Path to a text file
        delimiter (string): The character that delimits the analogy terms
        """
        self.delimiter = delimiter
        self.analogies = []
        with open(path, encoding="utf-8") as file:
            for line in file:
                if line[0] is not ':':
                    tokens = [preprocess(token.strip()) for token in line.split(delimiter)]
                    vectors = [word_vectors[token] for token in tokens if token in word_vectors]
                    if len(vectors) == 4:
                        # append the label's (i.e. D the target of analogy recovery) vocabulary index
                        vectors.append(word_vectors.vocab[tokens[3]].index)
                        self.analogies.append(vectors)

    def __len__(self):
        return len(self.analogies)

    def __getitem__(self, idx):
        return self.analogies[idx]



def extract_diachronic_vectors(line, word_vectors_t0, word_vectors_t1, delimiter, preprocess):
    if not line.startswith(':'):
        tokens = [preprocess(token.strip()) for token in line.split(delimiter)]
        vectors = [word_vectors_t0[token] for token in tokens[:2] if token in word_vectors_t0]
        vectors += [word_vectors_t1[token] for token in tokens[2:] if token in word_vectors_t1]
        if len(vectors) == 4:
            vectors.append(word_vectors_t1.vocab[tokens[3]].index) # append the label's (i.e. D the target of analogy recovery) vocabulary index
            return vectors
        else:
            return []
    else:
        return []

def iterate_textfiles(dir_path, except_files=[]):
    for dataset in os.scandir(dir_path):
        if dataset.name not in except_files:
            with open(dataset.path, 'r', encoding='utf-8') as f:
                for line in f:
                    yield line

"""
Loads a diachronic analogy data set. Expects a single file with four analogy terms per line, each
delimited by a delimiter symbol. The analogy is expected to be in
"A is to B as C is to D" order with A, B belonging to word_vectors_t0 and C, D belonging to word_vectors_t1.
Lines starting with a colon are filtered out.
"""
class DiachronicAnalogies(Dataset):
    def __init__(self,
                 path,
                 word_vectors_t0,
                 word_vectors_t1,
                 delimiter=' ',
                 except_files = [],
                 preprocess=lambda token: token):
        """
        Args:
        path (string): Path to a text file or path to a directory if except files is given
        delimiter (string): The character that delimits the analogy terms
        """
        self.delimiter = delimiter
        self.analogies = []
        if except_files:
            for line in iterate_textfiles(path, except_files=except_files):
                vectors = extract_diachronic_vectors(line, word_vectors_t0, word_vectors_t1, delimiter, preprocess)
                if vectors:
                    self.analogies.append(vectors)
        else:
            with open(path, encoding="utf-8") as file:
                for line in file:
                    vectors = extract_diachronic_vectors(line, word_vectors_t0, word_vectors_t1, delimiter, preprocess)
                    if vectors:
                        self.analogies.append(vectors)
    def __len__(self):
        return len(self.analogies)

    def __getitem__(self, idx):
        return self.analogies[idx]
