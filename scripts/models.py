import torch
import torch.nn as nn

"""Implements a simple baseline that just returns the known word
of the analogy target (i.e. the third vector). Therefore, there is no training.
"""
class TargetWord(nn.Module):
    def  __init__(self):
        super(TargetWord, self).__init__()
    def forward(self, a, b, c):
        assert a.size() == b.size()
        assert b.size() == c.size()

        return c


"""Implements a simple learnable transformation"""
class Transformation(nn.Module):
    def  __init__(self, dims):
        super(Transformation, self).__init__()
        self.weights = nn.Parameter(torch.eye(dims, dims).type(torch.FloatTensor))
    def forward(self, a):
        y = a.matmul(self.weights)
        return y


"""Implements the offset method.
There is no training as only simple vector arithmetic is used.
"""
class OffsetMethod(nn.Module):
    def  __init__(self):
        super(OffsetMethod, self).__init__()
    def forward(self, a, b, c):
        assert a.size() == b.size()
        assert b.size() == c.size()

        y = b - a + c

        return y


"""Implements the offset method with weights.
Applies learnable weights to the result of the offset."""
class WeightedOffset(nn.Module):
    def  __init__(self, dims):
        super(WeightedOffset, self).__init__()
        self.weights = nn.Parameter(torch.eye(dims, dims).type(torch.FloatTensor))
    def forward(self, a, b, c):
        assert a.size() == b.size()
        assert b.size() == c.size()

        y = b - a + c
        y = y.matmul(self.weights)

        return y

"""Implements the offset method with weights.
Applies learnable weights to the vectors in the offset."""
class IndividuallyWeightedOffset(nn.Module):
    def  __init__(self, dims, random = False):
        super(IndividuallyWeightedOffset, self).__init__()
        if random:
            self.weights_a = nn.Parameter(torch.eye(dims, dims).type(torch.FloatTensor) + 0.1 * torch.randn(dims, dims).type(torch.FloatTensor))
            self.weights_b = nn.Parameter(torch.eye(dims, dims).type(torch.FloatTensor) + 0.1 * torch.randn(dims, dims).type(torch.FloatTensor))
            self.weights_c = nn.Parameter(torch.eye(dims, dims).type(torch.FloatTensor) + 0.1 * torch.randn(dims, dims).type(torch.FloatTensor))
        else:
            self.weights_a = nn.Parameter(torch.eye(dims, dims).type(torch.FloatTensor))
            self.weights_b = nn.Parameter(torch.eye(dims, dims).type(torch.FloatTensor))
            self.weights_c = nn.Parameter(torch.eye(dims, dims).type(torch.FloatTensor))
    def forward(self, a, b, c):
        assert a.size() == b.size()
        assert b.size() == c.size()

        y = b.matmul(self.weights_b) - a.matmul(self.weights_a) + c.matmul(self.weights_c)
        return y
