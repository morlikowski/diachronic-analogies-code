import os

def iter_load_sentences(start_year, end_year, data_path):
    for year in range(start_year, end_year + 1):
            year_path = os.path.join(data_path, str(year)+'.txt')
            with open(year_path, 'r') as f:
                for sentence in f:
                    if sentence.split():
                        yield sentence.split()

class TimestampedSentences():
    def __init__(self, start_year, end_year, data_path):
        self.start_year = start_year
        self.end_year = end_year
        self.data_path = data_path
    def __iter__(self):
        return iter_load_sentences(self.start_year, self.end_year, self.data_path)
