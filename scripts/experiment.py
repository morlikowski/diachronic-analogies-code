import torch.utils.data
import torch.utils.data.sampler

from train import train, save_model
from eval import evaluate, load_model
from datasets import DiachronicAnalogies

from gensim.models import KeyedVectors
import configparser
import sys
import os

from math import floor

def folds(K, N):
    split = floor(N / K)
    for k in range(K):
        indecies_train = [i for i in range(split*(k+1), N)] + [i for i in range(0, split*k)]
        indecies_test = [i for i in range(split*k, split*(k+1))]
        yield indecies_train, indecies_test

def run_experiment(config_path):
    config = configparser.ConfigParser()
    config.read(config_path)

    paths = config['PATHS']
    MODEL_PATH = paths['weights']
    EMBEDDINGS_T0_PATH = paths['embeddings_t0']
    EMBEDDINGS_T1_PATH = paths['embeddings_t1']
    DATA_PATH = paths['data']
    RESULTS_PATH = paths['results']
    EXCEPT_PATHS = paths['except'].split()

    hyperparameters = config['HYPERPARAMETERS']
    EPOCHS = hyperparameters.getint('epochs')
    MODEL_NAME = hyperparameters.get('model')

    # get trained embeddings
    embeddings_t0 = KeyedVectors.load(EMBEDDINGS_T0_PATH)
    embeddings_t1 = KeyedVectors.load(EMBEDDINGS_T1_PATH)

    #precompute L2-normalized vectors and remove original vectors to free up memory
    embeddings_t0.init_sims(replace=True)
    embeddings_t1.init_sims(replace=True)

    #If there are files excluded from training, then all other files are used in training
    #and the last excluded file is used for evaluation (used for zero-shot-like evaluation)
    if EXCEPT_PATHS:
        print('Zero-shot setup')
        train_data = DiachronicAnalogies(DATA_PATH, embeddings_t0, embeddings_t1, except_files=EXCEPT_PATHS)
        test_data = DiachronicAnalogies(os.path.join(DATA_PATH, EXCEPT_PATHS[-1]), embeddings_t0, embeddings_t1)
        data = [(train_data, test_data)]
        N_train = len(train_data)
        N_test = len(test_data)
        N = N_test + N_train
        train_set = torch.utils.data.DataLoader(train_data,
                                                batch_size=4,
                                                shuffle=False,
                                                num_workers=3)
        test_set = torch.utils.data.DataLoader(test_data,
                                               batch_size=4,
                                               shuffle=False,
                                               num_workers=3)
    else:
        print('Cross-validation setup')
        raw_data = DiachronicAnalogies(DATA_PATH, embeddings_t0, embeddings_t1)
        K = 5
        N = len(raw_data)
        N_test = floor(N / K)
        N_train = N - N_test
        data = []
        for train_indecies, test_indecies in folds(K, N):
            train_set = torch.utils.data.DataLoader(raw_data,
                                                    batch_size=4,
                                                    shuffle=False,
                                                    num_workers=4,
                                                    #Random sampling, but only from the given indecies
                                                    sampler=torch.utils.data.sampler.SubsetRandomSampler(train_indecies))
            test_set = torch.utils.data.DataLoader(raw_data,
                                                   batch_size=4,
                                                   shuffle=False,
                                                   num_workers=1,
                                                   sampler=torch.utils.data.sampler.SubsetRandomSampler(test_indecies))
            data.append((train_set, test_set))

    for k, (train_set, test_set) in enumerate(data, start=1):
        print("Length of complete dataset: {0}".format(N))
        if N_test < 1 or N_train < 1:
            print('Not enough data for training.')
            return
        print("Training examples: {0}".format(N_train))
        print("Test examples: {0}".format(N_test))

        print("Training model with path {0}".format(MODEL_PATH))
        model = train(train_set, MODEL_PATH, epochs=EPOCHS, model_name=MODEL_NAME)
        save_model(model, MODEL_PATH)

        print("Evaluating...")
        results = evaluate(test_set, embeddings_t1, model)

        acc = results['correct'] / (N_test)
        print("Correct: {0}".format(results['correct']))
        print("N_test: {0}".format(N_test))
        print("Model ACC: {0:.2f}%".format(acc * 100))
        print("Model MRR: {0:.2f}%".format(results['mean_reciprocal_rank'] * 100))
        print("Model MSE: {0:.2f}".format(results['mean_squared_error']))
        print("Model COS: {0:.4f}".format(results['mean_cosine_similarity']))

        with open(RESULTS_PATH, 'a') as f:
            result_strings = ["{0:.2f}".format(metric) for metric in
                              [results['mean_reciprocal_rank'] * 100,
                               acc * 100,
                               results['mean_squared_error'],
                               results['mean_cosine_similarity']]]
            result_strings += [str(value) for value in
                               [MODEL_NAME, N_train, N_test, EPOCHS, os.path.basename(DATA_PATH), os.path.basename(EMBEDDINGS_T0_PATH)]]
            if EXCEPT_PATHS:
                result_strings += EXCEPT_PATHS[:-1]
            else:
                result_strings += [str(k)]
            print("\t".join(result_strings), file=f, sep=os.linesep)


if __name__ == "__main__":
    config_path = sys.argv[1]
	print(config_path)
	
	with open('results.tsv', 'w') as f:
	    print("\t".join(['MRR', 'ACC', 'MSE', 'COS', 'MODEL', 'N_train', 'N_test', 'EPOCHS', 'DATA', 'EMBEDDINGS', '0SHOT/K']),
	          file=f,
	          sep=os.linesep)
	
	if config_path == 'all':
	    for config_file in os.scandir('configs/'):
	        run_experiment(config_file.path)
	else:
	    run_experiment(config_path)
