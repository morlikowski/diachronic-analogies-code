# coding: utf-8
import os
import spacy
import glob

nlp = spacy.blank('nl')
nlp.add_pipe(nlp.create_pipe('sentencizer'))

print('Pipeline: ', nlp.pipe_names)

def is_noise(token, remove_stopwords = True, remove_punctuation = True):
    token_flags = [token.is_stop, token.is_punct]
    do_flags = [remove_stopwords, remove_punctuation]
    flags = [token and do for token, do in zip(token_flags, do_flags) ]
    return any(flags)

def normalize(token, do_lemmatize = True, do_lower = True ):
    if do_lemmatize and do_lower:
        return token.lemma_.lower()
    if do_lemmatize:
        return token.lemma_
    if do_lower:
        return token.lower_

def extract_sentences(texts, do_lower = True, do_lemmatize= True, remove_stopwords = True, remove_punctuation = True):
    for doc in nlp.pipe(texts, batch_size=10000, n_threads = 4):
        yield [[normalize(token, do_lower, do_lemmatize) for token in sent
                if not is_noise(token, remove_stopwords, remove_punctuation)]
                for sent in doc.sents]


def preprocess_files(paths):
    # Preprocess articles grouped in textfiles per year (one article per line) to single files with all sentences per year
    for path in paths:
        with open(path, 'r') as in_file:
            out_path = "{0}.done".format(path)
            try:
                os.remove(out_path)
            except OSError:
                pass
            with open(out_path, 'a+') as out_file:
                articles = (article for article in in_file)
                for sentences in extract_sentences(articles, do_lemmatize = False):
                    print(*[' '.join(sentence) for sentence in sentences if sentence],
                          file=out_file,
                          sep=os.linesep)

# Preprocess all articles to single files with all sentences per year
def preprocess_dir(path):
    for year in os.scandir(path):
        if year.is_dir and year.name in [str(year) for year in range(1980, 1990)]:
            out_path = "{0}.done".format(year.name)
            with open(out_path, 'w+') as out_file:
                articles = (open(article.path).read() for article in os.scandir(year.path) if article.name.endswith('.txt'))
                for sentences in extract_sentences(articles, do_lemmatize=False):
                    print(*[' '.join(tokens) for tokens in sentences if tokens],
                          file=out_file,
                          sep=os.linesep)


#preprocess_files(glob.glob('../data/articles/195*.txt'))    
preprocess_dir('../data/articles/')
