import scrapy
import json
import os
from datetime import datetime

class FulltextsSpider(scrapy.Spider):
    name = "fulltexts"

    def url_to_id(self, url):
        splits = url.split(':')
        return "{0}_{1}".format(splits[2], splits[4])

    def start_requests(self):
        with open('../data/articles_sample_50s.jl', 'r') as f:
            for article_string in f:
                article = json.loads(article_string)
                date = datetime.strptime(article['date'], '%Y/%m/%d %H:%M:%S')
                # article['id'] is the URL identifier of the ocr'ed fulltext
                request = scrapy.Request(url=article['id'], callback=self.parse)
                request.meta['year'] = date.year
                request.meta['id'] = self.url_to_id(article['id'])
                yield request

    def parse(self, response):
        year = str(response.meta['year'])
        id = response.meta['id']
        paragraphs = response.selector.xpath('//text/p/text()').extract()
        path = '../data/articles/{0}.txt'.format(year)
        with open(path, 'a') as f:
            print(' '.join(paragraphs), file=f, sep=os.linesep)
