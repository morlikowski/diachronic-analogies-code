import scrapy

class ArticlesSpider(scrapy.Spider):
    name = "articles"

    def start_requests(self):
        with open('../data/urls_50s.txt', 'r') as f:
            for url in f:
                yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        response.selector.register_namespace('srw', "http://www.loc.gov/zing/srw/")
        response.selector.register_namespace('dc', "http://purl.org/dc/elements/1.1/")
        ids = response.selector.xpath('//srw:records/*/srw:recordData/dc:identifier/text()').extract()
        dates = response.selector.xpath('//srw:records/*/srw:recordData/dc:date/text()').extract()
        for identifier, date in zip(ids, dates):
            yield {
                'id': identifier,
                'date': date
            }
